﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Core.Services;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Repositories;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {

        private readonly ICustomerService _customerServices;
        public UsersController(ICustomerService customerServices)
        {
            _customerServices = customerServices;
        }

        // GET: /Users
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result =  _customerServices.GetAllCustomers();

            if (result.Count > 0)
            {
                return Ok(JsonSerializer.Serialize(result));
            }

            return NoContent();

        }

        // GET: /Users/{id}
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(int id)
        {
            Customer result = await _customerServices.GetCustomerAsync(id);

            if (result == null)
            {
                return NotFound("User not found");
            }

            return Ok(result.ToString());
        }


        [HttpPost]
        public async Task<IActionResult> Post(Customer customer)
        {

            if (await _customerServices.CustomerExistsAsync(customer.Id))
            {
                return new StatusCodeResult((int)HttpStatusCode.Conflict);
            }

            await _customerServices.AddCustomerAsync(customer);

            return Ok();

        }


    }
}
