using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Services;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Repositories;
using Otus.Teaching.Concurrency.Import.WebApi;
using Otus.Teaching.Concurrency.Import.WebApi.Controllers;
using Xunit;

namespace XUnitTestProject1
{
    public class IntegrationTest : IClassFixture<WebApplicationFactory<Otus.Teaching.Concurrency.Import.WebApi.Startup>>
    {
  
        private readonly ICustomerService _customerService;
        private UsersController _controller;
        public IntegrationTest(WebApplicationFactory<Startup> factory)
        {
            _customerService = new FakeCustomerService();
            _controller = new UsersController(_customerService);
        }

        [Fact]
        public async Task Get_AllCustomers_ReturnsOkResult()
        {

            var OkResult = _controller.Get();
            //Assert  
            Assert.IsType<OkObjectResult>(OkResult.Result);
        }

        [Fact]
        public async Task Get_UserById_ReturnsOkResult()
        {

            var OkResult = _controller.Get(1);
            //Assert  
            Assert.IsType<OkObjectResult>(OkResult.Result);
        }

        [Fact]
        public async Task Get_UserById_ReturnsNotFoundResult()
        {
            var notFoundResult = _controller.Get(4);
            //Assert  
            Assert.IsType<NotFoundObjectResult>(notFoundResult.Result);
        }

        [Fact]
        public async Task Post_CreateCustomer_ReturnsConflictStatusCode()
        {
            Customer customer = new Customer()
            {
                Id = 1,
                FullName = "Petya",
                Email = "petya@mail.com",
                Phone = "123456789"
            };

            var conflict = _controller.Post(customer);

            var value = conflict.Result as StatusCodeResult;
            //Assert  
            Assert.IsType<StatusCodeResult>(conflict.Result);
            Assert.Equal(409, value.StatusCode);
        }

        [Fact]
        public async Task Post_CreateCustomer_ReturnOkStatusCode()
        {
            Customer customer = new Customer()
            {
                Id = 4,
                FullName = "Petya2",
                Email = "petya@mail.com",
                Phone = "123456789"
            };

            var resultOk = _controller.Post(customer);

            var value = resultOk.Result as StatusCodeResult;
            //Assert  
            Assert.IsType<OkResult>(resultOk.Result);
        }
    }
    
}
