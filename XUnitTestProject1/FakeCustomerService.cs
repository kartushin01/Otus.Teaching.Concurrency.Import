﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Services;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace XUnitTestProject1
{
   public class FakeCustomerService : ICustomerService
   {

       private  List<Customer> _customersList;

       public FakeCustomerService()
       {
           _customersList = new List<Customer>()
           {
               new Customer(){Id = 1, FullName = "Petya", Email = "petya@mail.com", Phone = "123456789"},
               new Customer(){Id = 2, FullName = "Ivan", Email = "ivan@mail.com", Phone = "123456789"},
               new Customer(){Id = 3, FullName = "Mary", Email = "mary@mail.com", Phone = "123456789"}
           };
       }

       public async Task<Customer> GetCustomerAsync(int id)
       {
           return  _customersList.FirstOrDefault(d => d.Id == id);
       }
        public List<Customer> GetAllCustomers()
        {
            return _customersList;
        }

        public async Task<List<Customer>> GetAllCustomersAsync()
        {
            return  _customersList;
        }

        public async Task AddCustomerAsync(Customer customer)
        {
            _customersList.Add(customer);
        }

        public async Task<bool> CustomerExistsAsync(int id)
        {
            return _customersList.Any(i => i.Id == id);
        }
   }
}
