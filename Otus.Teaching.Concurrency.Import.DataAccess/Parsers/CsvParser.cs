﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
   public class CsvParser : IDataParser<List<Customer>>
    {
       
        private string Path { get; set; }
        public CsvParser(string path)
        {
            Path = path;
        }
        public async Task<List<Customer>> ParseAsync()
        {
            List<Customer> result;

            using (var reader = new StreamReader(Path))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.PrepareHeaderForMatch = (string header, int index) => header.ToLower();
                result = csv.GetRecords<Customer>().ToList();
            }

            return result;
        }
    }
}
