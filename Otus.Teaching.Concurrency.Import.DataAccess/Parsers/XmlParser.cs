﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private string Path { get; set; }

        private XmlSerializer serializer;

        public XmlParser(string path)
        {
            Path = path;
            serializer = new XmlSerializer(typeof(CustomersList));
        }
        public async Task<List<Customer>> ParseAsync()
        {
            CustomersList customers = new CustomersList();
            if (File.Exists(Path))
            {
                using (FileStream reader = new FileStream(Path, FileMode.Open))
                {
                    customers = (CustomersList)serializer.Deserialize(reader);
                }
            }
            else
            {
                return null;
            }
            
            return customers.Customers;
        }
    }
}