﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class DbInitializer
    {
        private readonly CustomerContext _dataContext;
        public DbInitializer(CustomerContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task Initialize()
        {
            _dataContext.Database.EnsureCreated();
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.MigrateAsync();
        }
    }

}
