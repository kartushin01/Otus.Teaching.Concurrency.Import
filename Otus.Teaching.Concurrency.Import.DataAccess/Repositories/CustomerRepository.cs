using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        private readonly CustomerContext _dbContext;

        public CustomerRepository(CustomerContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddCustomerAsync(Customer customer)
        {
            Console.WriteLine($"{Thread.CurrentThread.Name} AddCustomerAsync Call");
            await _dbContext.Customers.AddAsync(customer);
        }

        public async Task AddRangeCustomerAsync(List<Customer> customers)
        {
            Console.WriteLine($"{Thread.CurrentThread.Name} AddRangeCustomerAsync Call");
            await _dbContext.Customers.AddRangeAsync(customers);
        }

        public async Task SaveAsync()
        {
            try
            {
                await _dbContext.SaveChangesAsync();

                Console.WriteLine($"{Thread.CurrentThread.Name} SaveAsync Call");
            }
            catch (Exception e)
            {
                Console.WriteLine($"{Thread.CurrentThread.Name} SaveAsync Call" );

                await _dbContext.SaveChangesAsync();
            }


        }

        public async Task<bool> CustomerExistsAsync(int id)
        {
            return await _dbContext.Customers.AnyAsync(i => i.Id == id);
        }

        public Customer GetCustomer(int id)
        {
            return _dbContext.Customers.FirstOrDefault(c => c.Id == id);
        }

        public void AddCustomer(Customer customer)
        {
            _dbContext.Customers.Add(customer);
            Console.WriteLine($"{Thread.CurrentThread.Name} AddCustomer Call");
        }

        public void AddRangeCustomer(List<Customer> customers)
        {
            _dbContext.AddRange(customers);
            Console.WriteLine($"{Thread.CurrentThread.Name} AddRangeCustomer Call");
        }

        public void Save()
        {
            try
            {
                _dbContext.SaveChanges();
                Console.WriteLine($"{Thread.CurrentThread.Name} Save Call");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception {Thread.CurrentThread.Name}");
                _dbContext.SaveChanges();
            }
           
        }

        public List<Customer> CustomersAll()
        {
            return _dbContext.Customers.ToList();
        }

        public int GetCount()
        {
            return _dbContext.Customers.Count();
        }

        public async Task<int> GetCountAsync()
        {
           return await _dbContext.Customers.CountAsync();
        }

        public async Task<List<Customer>> CustomersAllAsync()
        {
            return await _dbContext.Customers.ToListAsync();
        }

        public async Task<Customer> GetCustomerAsync(int id)
        {
            return await _dbContext.Customers.FirstOrDefaultAsync(c =>
                c.Id == id);
        }
    }
}