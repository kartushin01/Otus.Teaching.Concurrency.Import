﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Services
{
    public interface IHistoryService
    {
        void Add(Dictionary<int, DateTime> data);

        void RefreshHysory();

        bool IsPopulare(int id);


    }

    public class HistoryService : IHistoryService
    {

        public HistoryService()
        {
            HistoryList = new List<Dictionary<int, DateTime>>();
        }

        private List<Dictionary<int, DateTime>> HistoryList { get; set; }

        public void Add(Dictionary<int, DateTime> data)
        {
            HistoryList.Add(data);
        }

        public void RefreshHysory()
        {
            HistoryList = HistoryList.Where(c => c.Values.Equals(DateTime.Today)).ToList();
        }

        public bool IsPopulare(int id)
        {

            var res = HistoryList.Select(c => c.Keys.Equals(id) && c.Values.Equals(DateTime.Today)).ToList();

            if (res.Count > 2)
            {
                return true;
            }

            return false;

        }
    }
}
