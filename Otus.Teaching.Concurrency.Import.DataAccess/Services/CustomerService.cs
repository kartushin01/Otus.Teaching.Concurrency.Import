﻿using Microsoft.Extensions.Caching.Memory;
using Otus.Teaching.Concurrency.Import.Core.Services;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMemoryCache _cahe;
        private readonly IHistoryService _history;
        public CustomerService(ICustomerRepository customerRepository, IMemoryCache cahe, IHistoryService history)
        {
            _customerRepository = customerRepository;
            _cahe = cahe;
            _history = history;
        }

        public async Task<Customer> GetCustomerAsync(int id)
        {
            Customer customer = null;

            if (!_cahe.TryGetValue(id, out customer))
            {
                customer = await _customerRepository.GetCustomerAsync(id);
                if (customer != null)
                {
                    SetCahe(customer);
                }
            }

            Dictionary<int, DateTime> data = new Dictionary<int, DateTime>();

            data.Add(id, DateTime.Now);
            _history.Add(data);

            return customer;
        }

        public void EvictionCallback(object key, object value, EvictionReason reason, object state)
        {
            Customer customer = value as Customer;

            if (_history.IsPopulare(customer.Id))
            {
                SetCahe(value as Customer);
            }

        }

        public void SetCahe(Customer customer)
        {
            _history.RefreshHysory();

            var options = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(10))
                .RegisterPostEvictionCallback(callback: EvictionCallback);

            _cahe.Set(customer.Id, customer, options);
        }

        public async Task<List<Customer>> GetAllCustomersAsync()
        {
            return await _customerRepository.CustomersAllAsync();
        }

        public async Task AddCustomerAsync(Customer customer)
        {
           await _customerRepository.AddCustomerAsync(customer);
           await _customerRepository.SaveAsync();
        }

        public async Task<bool> CustomerExistsAsync(int id)
        {
            return await _customerRepository.CustomerExistsAsync(id);
        }

        public List<Customer> GetAllCustomers()
        {
            return _customerRepository.CustomersAll();
        }
    }
}
