﻿using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientApp
{
    class Program
    {
        private static string _requestUrl = "http://localhost:14621/users";
        static List<Customer> _customers = new List<Customer>();
        private static HttpClient _client = new HttpClient();
        static async Task Main(string[] args) => await Go();

        private static async Task Go()
        {
            int select = ShowMenu();

            switch (select)
            {
                case 1:
                    await GetUsers();
                    break;
                case 2:
                    await GetUser();
                    break;
                default:
                    await GenerateUSer();
                    break;
            }
        }

        private static async Task GenerateUSer()
        {

            var user = await RandomCustomerGenerator.GenerateRandomIdAsync(1);
            Console.WriteLine("Создан пользователь: " + user.First().ToString());

            var request = new HttpRequestMessage(HttpMethod.Post, _requestUrl);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Accept",
                "application/json");

            string jsonInString = JsonConvert.SerializeObject(user);

            var response = await client.PostAsync(_requestUrl, new StringContent(jsonInString));

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("User saved in db");
            }
            else
            {
                Console.WriteLine("Error: " + response.StatusCode);
            }

            await Go();
        }

        private static async Task GetUsers()
        {

            var request = new HttpRequestMessage(HttpMethod.Get, _requestUrl);
            using var response = await _client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var responseStream = await response.Content.ReadAsStringAsync();
                _customers = JsonConvert.DeserializeObject<List<Customer>>(responseStream);

                foreach (var user in _customers)
                {
                    Console.WriteLine(user.ToString());
                }
            }
            else
            {
                Console.WriteLine("Server say: " + response.StatusCode);
            }
            await Go();
        }

        private static async Task GetUser()
        {
            Console.WriteLine("Введите id");
            string id = Console.ReadLine();
            var request = new HttpRequestMessage(HttpMethod.Get, _requestUrl + $"/{id}");
            var response = await _client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var responseStream = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Server say: " + response.StatusCode);
                Console.WriteLine(responseStream);
            }
            else
            {
                Console.WriteLine("Server say: " + response.StatusCode);
            }
            await Go();
        }

        private static int ShowMenu()
        {
            Console.WriteLine("Что делаем:");
            Console.WriteLine("Получить всех: нажмите 1");
            Console.WriteLine("Получить пользователя: нажмите 2");
            Console.WriteLine("Сгенерировать и записать пользователя в бд: нажмите 3");
            return int.Parse(Console.ReadLine());
        }


        private async static Task SendPost(Customer customer)
        {
            using var request = new HttpRequestMessage(HttpMethod.Post, _requestUrl);
            var json = JsonConvert.SerializeObject(customer);
            using var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            request.Content = stringContent;
            var response = await _client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("User saved in db");
            }
            else
            {
                Console.WriteLine("Error: " + response.StatusCode);
            }


        }
    }
}
