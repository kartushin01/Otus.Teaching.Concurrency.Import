using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Repositories
{
    public interface ICustomerRepository
    {
        Task AddCustomerAsync(Customer customer);
        Task AddRangeCustomerAsync(List<Customer> customers);
        Task SaveAsync();
        Task<int> GetCountAsync();

        Task<List<Customer>> CustomersAllAsync();
        Task<Customer> GetCustomerAsync(int id);

        Task<bool> CustomerExistsAsync(int id);

        Customer GetCustomer(int id);
        void AddCustomer(Customer customer);
        void AddRangeCustomer(List<Customer> customers);
        void Save();
        List<Customer> CustomersAll();
        int GetCount();
    }
}