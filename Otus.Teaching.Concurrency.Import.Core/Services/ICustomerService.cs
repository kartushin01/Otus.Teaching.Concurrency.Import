﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Services
{
    public interface ICustomerService
    {
        Task<Customer> GetCustomerAsync(int id);

        List<Customer> GetAllCustomers();

        Task<List<Customer>> GetAllCustomersAsync();

        Task AddCustomerAsync(Customer customer);

        Task<bool> CustomerExistsAsync(int id);
    }
}
