using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bogus;
using Bogus.DataSets;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public static class RandomCustomerGenerator
    {
        public static async Task<List<Customer>> GenerateAsync(int dataCount)
        {
            var customers = new List<Customer>();
            var customersFaker =  await CreateFakerAsync(false);

            foreach (var customer in customersFaker.GenerateForever())
            {
                if (dataCount == customer.Id)
                    return customers;
                
                customers.Add(customer);
            }

            return customers;
        }

        public static async Task<List<Customer>> GenerateRandomIdAsync(int dataCount)
        {
            var customers = new List<Customer>();
            var customersFaker = await CreateFakerAsync(true);

            foreach (var customer in customersFaker.GenerateForever())
            {
                
                customers.Add(customer);
                if (dataCount == customers.Count)
                    return customers;
            }

            return customers;
        }

        private async static Task<Faker<Customer>> CreateFakerAsync(bool randomIds = false)
        {
            var id = (!randomIds) ? 1 : new Random().Next(1, 100000);

            var customersFaker = new Faker<Customer>()
                .CustomInstantiator(f => new Customer()
                {
                    Id = (!randomIds) ? id++ : new Random().Next(1,100000)
                })
                .RuleFor(u => u.FullName, (f, u) => f.Name.FullName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FullName))
                .RuleFor(u => u.Phone, (f, u) => f.Phone.PhoneNumber("1-###-###-####"));

            return customersFaker;
        }

  


    }
}