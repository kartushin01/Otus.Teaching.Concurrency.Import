﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Extensions;
using Otus.Teaching.Concurrency.Import.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class ThreadLoder : IDataLoader
    {

        private IServiceScopeFactory _scopeFactory;
        public ThreadLoder(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public async Task LoadDataAsync(List<Customer> customers, int countThreads)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            List<List<Customer>> Chunklists = ListExtensions.ChunkBy(customers, customers.Count / countThreads);

            Console.WriteLine("Lading start");

            int i = 1;
            foreach (List<Customer> list in Chunklists)
            {
                var thread = new Thread(() => Load(list))
                {
                    IsBackground = false,
                    Name = "MyThread " + i
                };

                thread.Start();
                thread.Join();
                i++;
            }

            stopWatch.Stop();

            using var scope = _scopeFactory.CreateScope();
            var repo = scope.ServiceProvider.GetRequiredService<ICustomerRepository>();

            Console.WriteLine($"write to the database {repo.GetCount()}");
            Console.WriteLine($"Handled queue in {stopWatch.Elapsed}...");
        }

        private void Load(List<Customer> data)
        {
            using var scope = _scopeFactory.CreateScope();
            var repo = scope.ServiceProvider.GetRequiredService<ICustomerRepository>();

            repo.AddRangeCustomer(data);
            repo.Save();

        }

    }
}
