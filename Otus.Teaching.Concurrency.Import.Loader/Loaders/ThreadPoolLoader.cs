﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Extensions;
using Otus.Teaching.Concurrency.Import.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class ThreadPoolLoader : IDataLoader
    {
        private IServiceScopeFactory _scopeFactory;

        public ThreadPoolLoader(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public async Task LoadDataAsync(List<Customer> customers, int countThreads)
        {

            List<List<Customer>> Chunklists = ListExtensions.ChunkBy(customers, customers.Count / countThreads);

            var SchedulerItemList = new List<ThreadPoolSchedulerItem>();

            var stopWatch = new Stopwatch();
            Console.WriteLine($"Thread pool scheduler in thread: {Thread.CurrentThread.ManagedThreadId}...");
            Console.WriteLine("Handling queue...");
            stopWatch.Start();

            foreach (var chunk in Chunklists)
            {
                var threadPoolSchedulerItem = new ThreadPoolSchedulerItem();
                threadPoolSchedulerItem.Chunk = chunk;
                SchedulerItemList.Add(threadPoolSchedulerItem);
                ThreadPool.QueueUserWorkItem(HandleInThreadPool, threadPoolSchedulerItem);
            }

            WaitHandle[] waitHandles = SchedulerItemList.Select(x => x.WaitHandle).ToArray();
            WaitHandle.WaitAll(waitHandles);
            stopWatch.Stop();
            Console.WriteLine($"Handled queue in {stopWatch.Elapsed}...");

            using var scope = _scopeFactory.CreateScope();
            var repo = scope.ServiceProvider.GetRequiredService<ICustomerRepository>();

            Console.WriteLine($"write to the database {repo.GetCount()}");


        }
        private async void HandleInThreadPool(object item)
        {
            var schedulerItem = item as ThreadPoolSchedulerItem;
            await LoadAsync(schedulerItem);
            var autoResetEvent = (AutoResetEvent)schedulerItem.WaitHandle;
            autoResetEvent.Set();
        }


        private async Task LoadAsync(ThreadPoolSchedulerItem data)
        {
            using var scope = _scopeFactory.CreateScope();
            var repo = scope.ServiceProvider.GetRequiredService<ICustomerRepository>();

            await repo.AddRangeCustomerAsync(data.Chunk);
            await repo.SaveAsync();


        }
    }
}
