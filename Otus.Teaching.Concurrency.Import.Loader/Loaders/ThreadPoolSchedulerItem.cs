﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class ThreadPoolSchedulerItem
    {
        
        public WaitHandle WaitHandle { get; private set; }
        public List<Customer> Chunk { get; set; }
        public ThreadPoolSchedulerItem()
        {
            WaitHandle = new AutoResetEvent(false);
        }

    }
}
