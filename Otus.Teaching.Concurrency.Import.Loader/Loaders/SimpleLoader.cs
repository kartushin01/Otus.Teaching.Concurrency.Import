﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Repositories;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class SimpleLoader : IDataLoader
    {

        private IServiceScopeFactory _scopeFactory;

        public SimpleLoader(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }
        public async Task LoadDataAsync(List<Customer> customers, int countThreads)
        {
            using var scope = _scopeFactory.CreateScope();
            var _repo = scope.ServiceProvider.GetRequiredService<ICustomerRepository>();

            var stopWatch = new Stopwatch();
            Console.WriteLine($"Simple load start");
            stopWatch.Start();

            foreach (var c in customers)
            {
               await _repo.AddCustomerAsync(c);
              
            }
            await _repo.SaveAsync();

            stopWatch.Stop();

            Console.WriteLine($"Well done:  {stopWatch.Elapsed}...");
            Console.WriteLine($"write to the database {_repo.GetCountAsync()}");
        }
    }
}
