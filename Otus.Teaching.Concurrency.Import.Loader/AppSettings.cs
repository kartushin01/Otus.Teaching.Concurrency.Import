﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class AppSettings
    {
        public bool UseProcess { get; set; }
        public string DataGeneratorPath { get; set; }
        public string OutputFileName { get; set; } 
        public int CountRecords { get; set; }
    }
}
