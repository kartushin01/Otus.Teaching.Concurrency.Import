﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using Otus.Teaching.Concurrency.Import.Repositories;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional:true, reloadOnChange:true);

            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CustomerContext>(ServiceLifetime.Transient);
            services.AddSingleton<IConfigurationRoot>(Configuration);
            services.AddTransient<IDataLoader, ThreadLoder>();
            services.AddTransient<ICustomerRepository, CustomerRepository>();
        }
    }
}
