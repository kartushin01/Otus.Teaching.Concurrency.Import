﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using CsvParser = Otus.Teaching.Concurrency.Import.DataAccess.Parsers.CsvParser;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static IServiceProvider _serviceProvider;

        private static string _dataFilePath;
        
        private static AppSettings _settings = new AppSettings(); 
        
        private static List<Customer> _data = new List<Customer>();

        private static DateTime _start;

        static async Task Main(string[] args)
        {

            IServiceCollection services = new ServiceCollection();

            Startup startup = new Startup();

            startup.ConfigureServices(services);

            _serviceProvider = services.BuildServiceProvider();

            _settings = _serviceProvider.GetService<IConfigurationRoot>().GetSection("AppSettings").Get<AppSettings>();

            await DatabaseInit();


            if (_settings.UseProcess && File.Exists(await GetPathToHandlerProcess(_settings)))
            {
                var outputDir = Directory
                    .GetParent(Directory.GetCurrentDirectory()).Parent
                    .Parent.Parent.FullName;

                var processStartInfo = new ProcessStartInfo()
                {
                    ArgumentList = { $"{Path.Combine(outputDir, _settings.OutputFileName)}", _settings.CountRecords.ToString() },
                    FileName = await GetPathToHandlerProcess(_settings),
                    UseShellExecute = true
                };

                _dataFilePath = Path.Combine(outputDir, _settings.OutputFileName);
                Process.Start(processStartInfo)?.WaitForExit();
            }
            else
            {
                string path = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName, _settings.OutputFileName);

                await GenerateCustomersDataFile(path, _settings.CountRecords);

                _dataFilePath = path;
            }

            Console.WriteLine("Start parse csv");

            _data = await ParseDataCsv(_dataFilePath);

            Console.WriteLine("Finish parse");

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            await _serviceProvider.GetRequiredService<IDataLoader>().LoadDataAsync(_data, 3);

        }

        private static async Task DatabaseInit()
        {
            using var dataContext = new CustomerContext();
            await new DbInitializer(dataContext).Initialize();
        }

        public static async Task<List<Customer>> ParseDataXml(string pathToFile)
        {
            var parser = new XmlParser(pathToFile);
            return await parser.ParseAsync();
        }
        public static async Task<List<Customer>> ParseDataCsv(string pathToFile)
        {
            var parser = new CsvParser(pathToFile);
            return await parser.ParseAsync();
        }

        static async Task GenerateCustomersDataFile(string path, int count)
        {
            var generator = new DataGenerator.Generators.CsvGenerator(path, count);
            await generator.GenerateAsync();
        }

        static async Task<string> GetPathToHandlerProcess(AppSettings settings)
        {
            var slnPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.Parent.FullName;
            return Path.Combine(slnPath, settings.DataGeneratorPath);
        }
    }


}